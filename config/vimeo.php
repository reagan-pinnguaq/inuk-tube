<?php

/*
 * This file is part of Laravel Vimeo.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Vimeo Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'client_id' => 'ab7166622786e0ebbeb0d8f25b063bf9b3a6cce6',
            'client_secret' => '5TJN4YYGrqT8urd1ITfFAR9PIkulxsSSoeZYe2Rp8WoTY35KPn40XGJbLPN88GxcBAKdjE4dZ6v6giQ7ogz7HhqJ5VTplbjHlSPiGvFBDjPjjcwgm3PfWb3MZirOe+aM',
            'access_token' => '32e8994c16708fe1679b9bb6e163453b',
        ],

    ],

];
