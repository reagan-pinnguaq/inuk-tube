<?php

use Illuminate\Database\Seeder;

class MusicReligious extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $youtube = array(
            "GTwaRKQtCfA",
            "juBHMJxTEpw",
            "ticZqAs_u3k",
            "NtNuELl5he0",
            "P6MBlcESkNs",
            "v5RL5NJB144",
            // "0fzpdw4m5w",
            "4OAvIx4VYts",
            "PR_AXgJ9Sco",
            "GHcuiH1N96Y"
        );
        foreach($youtube as $key){
            DB::table('videos')->insert([
                'type_id' => 1,
                'key' => $key,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()            
            ]);
            $result = \App\Video::where('key', $key)->first();
            $id = $result->id; 
            $fetchedInfo = $result->getContents();
            DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
            DB::table('video_categories')->insert([
                'video_id' => $id,
                'category_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
            DB::table('video_sub_categories')->insert([
                'video_id' => $id,
                'category_id' => '1',
                'sub_category_id' => '3',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
        }
    }
}
