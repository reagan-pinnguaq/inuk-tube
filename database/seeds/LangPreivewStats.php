<?php

use Illuminate\Database\Seeder;

class LangPreivewStats extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lang_contents')->insert([
            'field' => 'LangPreviewViews',
            'english' => 'views',
            'french' => 'vues',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
