<?php

use Illuminate\Database\Seeder;

class Knowledge extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $youtube = array(
            "8IqOegVCNKI",
            "V6_l5b6vmMQ",
            "OUwcDIyv_7U",
            "IVDtbz0j7jE",
            "WZSgqM-WF3E",
            "47bOny714Fw"
        );
        foreach($youtube as $key){
            DB::table('videos')->insert([
                'type_id' => 1,
                'key' => $key,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()            
            ]);
            $result = \App\Video::where('key', $key)->first();
            $id = $result->id; 
            $fetchedInfo = $result->getContents();
            DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
            DB::table('video_categories')->insert([
                'video_id' => $id,
                'category_id' => '5',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
    
            }
    }
}
