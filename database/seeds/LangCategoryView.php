<?php

use Illuminate\Database\Seeder;

class LangCategoryView extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lang_contents')->insert([
            'field' => 'LangCategoryViewMore',
            'english' => 'View More',
            'french' => 'afficher plus',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
