<?php

use Illuminate\Database\Seeder;

class Kids extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $youtube = array(
        "whjYYsnCkrs",
        "i4EBF6quflI",
        "-0r_gzeqHsA",
        "H9h7RKxITAg",
        "fusYZ7eIhps",
        "QSVm-Fg3ltI",
        "78iJwtoA1_M",
        "LDOfCvaR0bg",
        "ZxOnzbKks1w",
        "d6qaiSKEeXI"
    );
    foreach($youtube as $key){
        DB::table('videos')->insert([
            'type_id' => 1,
            'key' => $key,
            'status' => 1,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        $result = \App\Video::where('key', $key)->first();
        $id = $result->id; 
        $fetchedInfo = $result->getContents();
        DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
        DB::table('video_categories')->insert([
            'video_id' => $id,
            'category_id' => '2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()  
        ]);

        }
    }
}
