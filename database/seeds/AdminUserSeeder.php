<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            DB::table('users')->insert([
                'email' => 'reagan@pinnguaq.com' ,
                'password' => Hash::make( '!reageekdev' ) ,
                'name' => 'Reagan' 
            ]);
 
    }
}
