<?php

use Illuminate\Database\Seeder;

class MusicTraditional extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   

        $youtube = array(
            'irL8JjwLhXg',
            'Yo5uaikhdhI',
            'G2cJ6r6Z33k',
            'ibS6URrwnRQ',
            '62u0Pk_z1WU',
            '_wCQW67YPuw',
            'mBmKc2FZocA',
            'eYVFK3FWJ5o',
            'BAhQuXCrCcQ',
            'RKRs-Cu2QBA'
        );
        foreach($youtube as $key){
            DB::table('videos')->insert([
                'type_id' => 1,
                'key' => $key,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()            
            ]);
            $result = \App\Video::where('key', $key)->first();
            $id = $result->id; 
            $fetchedInfo = $result->getContents();
            DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
            DB::table('video_categories')->insert([
                'video_id' => $id,
                'category_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
            DB::table('video_sub_categories')->insert([
                'video_id' => $id,
                'category_id' => '1',
                'sub_category_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);

        }
        
    }
}
