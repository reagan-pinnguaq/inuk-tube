<?php

use Illuminate\Database\Seeder;

class SettingCategoryPreviewLimiter extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'field' => 'category_limit',
            'value' => '8',
            'description' => 'This field limits how many video will show up in the category archive preview page. This will only effect /categories'
        ]);
    }
}
