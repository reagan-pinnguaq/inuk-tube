<?php

use Illuminate\Database\Seeder;

class Community extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $youtube = array(
            "T8SK6lk9ZAg",
            "xfpWbfAPXVE",
            "NMKYz_1bqLU",
            "21sVKuVdprA",
            "mK4uZ2odJbU",
            "oVEu4wz18go"    
        );
        foreach($youtube as $key){
            DB::table('videos')->insert([
                'type_id' => 1,
                'key' => $key,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()            
            ]);
            $result = \App\Video::where('key', $key)->first();
            $id = $result->id; 
            $fetchedInfo = $result->getContents();
            DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
            DB::table('video_categories')->insert([
                'video_id' => $id,
                'category_id' => '6',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
    
            }
    }
}
