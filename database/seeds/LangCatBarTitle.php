<?php

use Illuminate\Database\Seeder;

class LangCatBarTitle extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lang_contents')->insert([
            'field' => 'LangCatBarTitle',
            'english' => 'categories',
            'french' => 'les categories',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
