<?php

use Illuminate\Database\Seeder;

class AddVideoPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'add-video',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now() 
        ]);

        $id = \App\Page::where('name', 'add-video')->first()->id;

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoTitle',
            'english' => 'add a video',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoDescription',
            'english' => 'Do you know of a video that should be on our site? Let us know then. Please use the very simple form below to let us know about the video!',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoLinkTitle',
            'english' => 'Link to the Video',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoHostedTitle',
            'english' => 'Where Is The Video Hosted',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoVerifyTitle',
            'english' => 'Solve The Following Question:',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoVerifyPlaceholder',
            'english' => 'Answer',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoAgreeTitle',
            'english' => 'Video Publishing Agreement (*)',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoAgreeBody',
            'english' => 'I understand and acknowledge that this video I am submitting may be published to Inuktitube web properties including (but not limited to) Inukitube, Facebook®, YouTube®, Twitter® and/or any other promotional methods determined by Inukitube. I understand and acknowledge that Inuktitube does not make any guarantees and is not contractually bound in any way to the success of the video with respect to views, shares or any other metrics related to the promotion of the video according to the terms of this agreement, which I have read and agree to.',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoTermTitle',
            'english' => 'Termination Clause (*)',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoTermBody',
            'english' => 'I understand and acknowledge that my participation in the Inukitube promotional video program is at will and I agree that this license may only be terminated by the mutual agreement between all parties.',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoConfirmTitle',
            'english' => 'Yes I understand and accept the terms above.',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangAddVideoUploadButton',
            'english' => 'Add Video',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
