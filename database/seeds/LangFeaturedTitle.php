<?php

use Illuminate\Database\Seeder;

class LangFeaturedTitle extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lang_contents')->insert([
            'field' => 'LangFeaturedTitle',
            'english' => 'featured videos',
            'french' => 'vidéos liées',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
