<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */    
    
    public function run()
    {   
        $english = array(
            "humor",
            "inspirational",
            "trailer",
            "love",
            "documentary",
            "suspense",
            "horror",
            "adventure",
            "poetry",
            "history",
            "interview",
            "hunting",
            "action",
            "family",
            "self-help",
            "historical",
            "spiritual",
            "science",
            "children",
            "spirituality",
            "travel",
            "comedy",
            "religion",
            "inspiration",
            "series",
            "relationships",
            "art",
            "business",
            "women",
            "men",
            "life",
            "health",
            "humour",
            "friendship",
            "politics",
            "education",
            "healing",
            "poetry",
            "story",
            "leadership",
            "success",
            "short-story",
            "sledding",
            "movie",
            "motivational",
            "food",
            "philosophy",
            "mythology",
            "parenting",
            "review",
            "nature",
            "culture",
            "speaker",
            "satire",
            "motivation",
            "cooking",
            "how-to",
            "freedoom",
            "school",
            "summer",
            "winter",
            "fall",
            "creative",
            "entertainment",
            "survival",
            "singing",
            "performance",
            "learning",
            "elder"
        );
        foreach($english as $entry){
            DB::table('tags')->insert([
                'english' => $entry,
                'french' => 'fr',
                'inuktitut' => 'i'
            ]);
        }
    }
}
