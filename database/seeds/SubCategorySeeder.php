<?php

use Illuminate\Database\Seeder;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'english' => 'Traditional',
            'french'  => 'Traditionnel',
            'inuktitut' => 'Inuit Nijjausingit',
        ]);
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'english' => 'Modern',
            'french'  => 'Moderne',
            'inuktitut' => 'Maanasiutiit Nijjautiit',
        ]);
        DB::table('sub_categories')->insert([
            'category_id' => '1',
            'english' => 'Religious',
            'french'  => 'Religieux',
            'inuktitut' => 'Pisiit',
        ]);
    }
}