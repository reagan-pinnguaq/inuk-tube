<?php

use Illuminate\Database\Seeder;

class SettingRecentLimit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'field' => 'recent_count',
            'value' => '5',
            'description' => 'This setting changes the limit of items that show up Recent videos section. The suggested default is 5'
        ]);
    }
}
