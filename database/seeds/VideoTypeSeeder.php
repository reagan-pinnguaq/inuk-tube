<?php

use Illuminate\Database\Seeder;

class VideoTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('video_types')->insert([
            'name' => 'youtube',
        ]);
        DB::table('video_types')->insert([
            'name' => 'vimeo',
        ]);
    }
}
