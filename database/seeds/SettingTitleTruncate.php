<?php

use Illuminate\Database\Seeder;

class SettingTitleTruncate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'field' => 'title_truncate',
            'value' => '57',
            'description' => 'This field limits how many characters will show up on a title in a preview format. Typcically this will only apply on /categoies/.'
        ]);
    }
}
