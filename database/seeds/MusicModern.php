<?php

use Illuminate\Database\Seeder;

class MusicModern extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $youtube = array(
           "AAJHGWKe7hw", 
           "Z0jSgLfdJio",
           "QJ9nsH6rAnk", 
           "H_kZEx2wRcw",
           "UwMOytqRxGo",
           "9ZbPf6kwEh0", 
           "gTkbQ6mZuTA",
           "FrfMiLMg_qI",
           "t6Pp5eJ0X7Q",
           "AFrvOp4zgJY"
        );
        foreach($youtube as $key){
            DB::table('videos')->insert([
                'type_id' => 1,
                'key' => $key,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()            
            ]);
            $result = \App\Video::where('key', $key)->first();
            $id = $result->id; 
            $fetchedInfo = $result->getContents();
            DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
            DB::table('video_categories')->insert([
                'video_id' => $id,
                'category_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
            DB::table('video_sub_categories')->insert([
                'video_id' => $id,
                'category_id' => '1',
                'sub_category_id' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
        }
    }
}
