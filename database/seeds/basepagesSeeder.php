<?php

use Illuminate\Database\Seeder;

class BasePagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
           'name' => 'privacy policy'
        ]);
        DB::table('pages')->insert([
            'name' => 'terms of service'
        ]);

        // DB::table('pages')->insert([
        //     'name' => ''
        // ]);
    }
}
