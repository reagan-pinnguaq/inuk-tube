<?php

use Illuminate\Database\Seeder;

class ConfirmVideoPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'confirm-video',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now() 
        ]);

        $id = \App\Page::where('name', 'confirm-video')->first()->id;

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangConfirmVideoTitle',
            'english' => 'Confirm The Video',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangConfirmVideoLabel',
            'english' => 'Yes, this is the video I expected.',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);

        DB::table('lang_contents')->insert([
            'page_id' => $id,
            'field' => 'LangConfirmVideoButton',
            'english' => 'Submit',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
