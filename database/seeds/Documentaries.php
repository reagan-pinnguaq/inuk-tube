<?php

use Illuminate\Database\Seeder;

class Documentaries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $youtube = array(
            "NK8YFA3G9JY",
            "Fw04UJtc7-E",
            "uB4JGUzve6M",
            "Kxdqjn1sFM8",
            "3Bul6hsbwA4" ,
            "M7gMwZjpaoY",
            "i3pGKMySfdg",
            "Xh7WL9OoGdo",
            "AR9uJAF4w9A",
            "bxlHu6hSycM"
        );
        foreach($youtube as $key){
            DB::table('videos')->insert([
                'type_id' => 1,
                'key' => $key,
                'status' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()            
            ]);
            $result = \App\Video::where('key', $key)->first();
            $id = $result->id; 
            $fetchedInfo = $result->getContents();
            DB::table('videos')->where('id', $id)->update(['name' => $fetchedInfo->title]);
            DB::table('video_categories')->insert([
                'video_id' => $id,
                'category_id' => '3',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()  
            ]);
    
            }
    }
}
