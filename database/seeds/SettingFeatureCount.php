<?php

use Illuminate\Database\Seeder;

class SettingFeatureCount extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'field' => 'feature_count',
            'value' => '5',
            'description' => 'This setting changes the limit of items that show up Featured videos section. The suggested default is 5'
        ]);
    }
}
