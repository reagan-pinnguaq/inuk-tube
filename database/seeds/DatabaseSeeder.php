<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $this->call(AdminUserSeeder::class);
        $this->call(VideoTypeSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(SubCategorySeeder::class);
        $this->call(TagSeeder::class);
    
        //Seed the videos and categories
        
            //Music
            $this->call(MusicTraditional::class);
            $this->call(MusicModern::class);
            $this->call(MusicReligious::class);

            //Kids
            $this->call(Kids::class);

            //Documentaries
            $this->call(Documentaries::class);

            // // //Inuit Knowledge
             $this->call(Knowledge::class);

            // //Dramatic
            $this->call(Dramatic::class);

            // //Community
            $this->call(Community::class);

            // //Personal
            $this->call(Personal::class);

        //Settings
        $this->call(SettingCategoryPreviewLimiter::class);
        $this->call(SettingTitleTruncate::class);
        $this->call(SettingFeatureCount::class);
        $this->call(SettingRecentLimit::class);
        $this->call(SettingVideoDescriptionPreview::class);
        $this->call(SettingPaginator::class);  
        
        //Content
        $this->call(LangNavigation::class);
        $this->call(LangFeaturedTitle::class);
        $this->call(LangCatBarTitle::class);
        $this->call(LangRecentTitle::class);
        $this->call(LangRelatedTitle::class);
        $this->call(LangCategoryView::class);
        $this->call(LangPreivewStats::class);

        $this->call(AddVideoPage::class);
        $this->call(ConfirmVideoPage::class);
        $this->call(BasePagesSeeder::class);           
            
        //Assign videos to categories.... 
        $this->call(VideoFeatureSeeder::class);
    }
}
