<?php

use Illuminate\Database\Seeder;

class LangNavigation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lang_contents')->insert([
            'field' => 'LangNavSearch',
            'english' => 'search',
            'french' => 'rechercher',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'field' => 'LangNavAddVideo',
            'english' => 'add video',
            'french' => 'ajouter une vidéo',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'field' => 'LangNavCategories',
            'english' => 'categories',
            'french' => 'les catégories',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
        DB::table('lang_contents')->insert([
            'field' => 'LangNavRandomVideo',
            'english' => 'Random',
            'french' => 'au hasa',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);

    }
}
