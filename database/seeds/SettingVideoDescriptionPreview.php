<?php

use Illuminate\Database\Seeder;

class SettingVideoDescriptionPreview extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'field' => 'video_description_length',
            'value' => '250',
            'description' => 'This field limits how many characters a preview description on a video will be..'
        ]);
    }
}
