<?php

use Illuminate\Database\Seeder;

class LangRelatedTitle extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('lang_contents')->insert([
            'field' => 'LangRelatedTitle',
            'english' => 'related videos',
            'french' => 'vidéos connexes',
            'inuktitut' => 'a',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()            
        ]);
    }
}
