<?php

use Illuminate\Database\Seeder;
class VideoFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\VideoFeature::seedVideoFeature();

        
    }
}
