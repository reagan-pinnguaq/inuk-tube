<?php

use Illuminate\Database\Seeder;

class SettingPaginator extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'field' => 'paginator',
            'value' => '16',
            'description' => 'This field adjusts how many results are shown per page before pagination takes effect. Suggested that this is a multiple of 4.'
        ]);
    }
}
