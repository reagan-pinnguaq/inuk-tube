<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'english' => 'Music',
            'french'  => 'La Musique',
            'inuktitut' => 'Nijjausijaqtunik',
        ]);
        DB::table('categories')->insert([
            'english' => 'Children',
            'french'  => 'Des Gamins',
            'inuktitut' => 'Surusiit',
        ]);
        DB::table('categories')->insert([
            'english' => 'Documentaries',
            'french'  => 'Documentaire',
            'inuktitut' => 'Tarrijaliataminiit',
        ]);
        DB::table('categories')->insert([
            'english' => 'Dramatic',
            'french'  => 'Dramatique',
            'inuktitut' => 'Pillarinnguaqtit',
        ]);
        DB::table('categories')->insert([
            'english' => 'Inuit Knowledge',
            'french'  => 'Connaissance Des Inuits',
            'inuktitut' => 'Ilinniataugilinnungajut',
        ]);
        DB::table('categories')->insert([
            'english' => 'Community',
            'french'  => 'Communauté',
            'inuktitut' => 'Nunaliit',
        ]);
        DB::table('categories')->insert([
            'english' => 'Personal',
            'french'  => 'Personnel',
            'inuktitut' => 'Namminiq',
        ]);
    }
}