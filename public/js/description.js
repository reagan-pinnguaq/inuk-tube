var clickable = document.getElementsByClassName('video-description-toggle')[0];
var actionable = document.getElementsByClassName('video-description');
var button = clickable.children[0];
var text = ["Show Less", "Show More"];

button.addEventListener("click", updateDescription);

function updateDescription(){
	actionable[0].style.display = actionable[0].style.display === 'none' ? 'block' : 'none';
	actionable[1].style.display = actionable[1].style.display === 'none' ? 'block' : 'none';
	button.textContent === 'Show Less' ? button.textContent = text[1] :  button.textContent = text[0];	
}
