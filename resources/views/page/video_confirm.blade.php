@extends('layouts.master') 

@section('title') Comfirm the Video @endsection
@section('description') Add a video to our site via an easy to use upate @endsection

@section('content')
<div class="content-wrapper">

	<h2 class="my-1">{{ucwords($title->$lang)}}</h2>
	<form action="/confirm" method="POST" >
	 <label for="confirm"><input type="checkbox" name="confirm" id="confirm"> {{ucwords($label->$lang)}}</label>
	 <input type="hidden" name="key" value="{{$video->key}}">
	 <input type="hidden" name="title" value="{{$video->title}}">
	 @include('partials.token')
	 <input type="submit" class="p-1" value="{{ucwords($button->$lang)}}">
	</form>	
	@include('partials.video.video')
	@include('partials.errors')
</div>
@endsection