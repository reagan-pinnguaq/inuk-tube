
@extends('layouts.master') 

@section('title') {{$video->title}} @endsection
@section('description')  @endsection

@section('content')
<div class="content-wrapper">
	@include('partials.video.video')
	
</div>
@include('partials.video.related')
@endsection