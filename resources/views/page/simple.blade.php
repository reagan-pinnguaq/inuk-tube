@extends('layouts.master') 

@section('title') {!! ucwords($page->field('page_title')) !!} @endsection
@section('description') {!! \App\Model::limitText(strip_tags($page->field('page_content')), 150) !!} @endsection

@section('content')
<div class="content-wrapper">
	<div class="row justify-content-center py-2">
		<h3 class="my-3">{!! ucwords($page->field('page_title')) !!}</h3>
	</div>

	<div class="editable editable-content my-1" id="page_content">
			{!! $page->field('page_content') !!}
	</div>

</div>

@endsection