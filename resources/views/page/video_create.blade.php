@extends('layouts.master') 

@section('title') {!! ucwords(\App\LangContent::where('field', 'LangAddVideoTitle')->first()->$lang) !!} @endsection
@section('description') {!! ucwords(\App\LangContent::where('field', 'LangAddVideoTitle')->first()->$lang) !!} @endsection

@section('content')
<div class="content-wrapper">

	<h2 class="my-1">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoTitle')->first()->$lang) !!}</h2>
		@include('partials.message')
	<form action="/upload" method="POST" >
	<div class="row">
		<div class="half">
		@include('partials.upload.explain')
		@include('partials.upload.source')
		@include('partials.upload.validator')
		</div>
		<div class="half">
		@include('partials.upload.policy')
		</div>
	</div>
	 
	 <input class="p-1" type="submit" value="{!! ucwords(\App\LangContent::where('field', 'LangAddVideoUploadButton')->first()->$lang) !!}">
	</form>
	@include('partials.errors')
</div>
@endsection