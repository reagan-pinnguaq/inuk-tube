@extends('layouts.master') 

@section('title') Searching For: {{$term}} @endsection
@section('description') A Search page that allowes you to find vidoes linked on the site. @endsection

@section('content')
<div class="content-wrapper">
	<form action="/search/" method="GET"><h2 class="my-1"> Search Results For: <input type="text" name="query" value="{{$term}}"></h2></form>
	@include('partials.search.video_list')
	@include('partials.search.category_list')
</div>
@endsection