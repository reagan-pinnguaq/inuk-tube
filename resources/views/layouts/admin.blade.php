@push('styles')
<link href="{{ asset('css/admin.css') }}" rel="stylesheet" type="text/css" >
@endpush
@include('partials.meta.header')
@include('admin.partials.nav')
@include('partials.page.header')
<div id="content">
	<div class="content-wrapper">
	@include('partials.message')
	</div>
	@yield('content')
	@include('partials.errors')
</div>
@include('partials.meta.footer')