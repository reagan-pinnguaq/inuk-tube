@push('styles')
<link href="{{ asset('css/admin.css') }}" rel="stylesheet" type="text/css" >
@endpush
@include('partials.meta.header')
	<div id="content">@yield('content')</div>
@include('partials.meta.footer')
