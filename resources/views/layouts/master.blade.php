@include('partials.page.session')
@include('partials.meta.header')
@include('partials.page.nav')
@include('partials.page.header')
<div id="content">
	@yield('content')
</div>
@include('partials.page.footer') 
@include('partials.meta.footer')