<div id="{{$videoData->id}}" class="video-preview row m-1">
	<a href="/video-manager/{{$videoData->id}}" class="full video-thumbnail-wrapper" >
		<div class="video-thumbnail" style="background-image:url('{{$video->thumbnail}}');"></div>
	</a>
	<h4 class="video-title full mt-1 px-0"><a href="/video-manager/{{$videoData->id}}">{!! ucwords(\App\Model::limitText($video->title, 57)) !!}</a></h4>
</div>