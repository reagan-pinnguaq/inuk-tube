<div class="tag-wrapper py-1">


@if(isset($videoData))
		@foreach($videoData->tags() as $dataTag)

		<input type="hidden" name="stored[tag][]" value="{{$dataTag->tag_id}}">
	
		@endforeach
		<input type="hidden" name="stored[tag][]" value="">
@endif
	<h5 class="my-1">Choose The Tags That Apply To This Video</h5>
	<div class="form-group checkboxes row my-1">
		@foreach($tags as $tag)
		<label for="{{$tag->english}}">
		<input type="checkbox" name="tag[{{$tag->id}}]" id="{{$tag->english}}"
		@if(isset($videoData))
		@foreach($videoData->tags() as $dataTag)
		@if($dataTag->tag_id == $tag->id)
		{{'checked'}}
		@endif
		@endforeach
		@endif
		>{{$tag->english}}</label>
		@endforeach
	</div>
</div>

