<nav id="main-nav" class="row">
	<ul class="row justify-content-start align-items-center content-wrapper">
		<li><a href="/dashboard">Dashboard</a></li>
		<li class="justify-right">
			@if($verify_count != 0)
			<span class="alert alert-danger">{{$verify_count}}</span>
			@endif
			<a href="/video-manager">Video Manager</a>
		</li>
		<li>
			<a href="/content-manager">Content Manager</a>
		</li>
		<li>
			<a href="/setting-manager">Setting Manager</a>
		</li>
		<li class="justify-right"><a href="/">Back to Site</a></li>
		<li>@include('auth.logout')</li>
		<!-- logout -->
	</ul>
</nav>
