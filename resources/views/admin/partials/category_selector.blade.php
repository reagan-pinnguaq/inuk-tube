<div class="category-selector-wrapper">

@if(isset($videoData))
		@foreach($videoData->categories() as $dataTag)

		<input type="hidden" name="stored[category][]" value="{{$dataTag->category_id}}">
	
		@endforeach
@endif
	<h5 class="my-1"><label for="category"> Choose Categories For The Video:</label></h5>
	<div class="form-group checkboxes row my-1" >
	@foreach($categories as $category)
	<label for="{{$category->english}}"><input  type="checkbox"
	name="category[{{$category->id}}]" id="{{$category->english}}"
	@if(isset($videoData))
	@foreach($videoData->categories() as $dataCategory)
	@if($dataCategory->category_id == $category->id)
	 {{'checked'}}
	@endif
	@endforeach
	@endif
	>{{$category->english}}</label>
	@endforeach
</div>
</div>
