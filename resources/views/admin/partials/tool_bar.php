<div class="toolbar my-2">
	<div class="change-language row align-items-center  no-wrap">
	<h5 class=" m-0 one-quarter">Change Language:</h5>
		<ul class="row mb-0 half">
				
				<li class="mx-2"><a class="text-upper" href="/language/english">en</a></li>
				<!-- <li><a class="text-upper" href="/language/french">fr</a></li> -->
				<li class="mx-2"><a class="text-upper" href="/language/inuktitut">in</a></li>
			</ul>
		<div class="one-third"> 
			<div id="editor-toolbox"></div>
		</div>
	</div>
</div>