<div class="video-status">
	<div class="row">
		<h5 class="my-1">Video Status</h5>
	</div>
	<label for="status">Currently:
		<select name="status" id="status">
			<option value="1" @if($videoData->status == true) selected @endif>enabled</option>
			<option value="0" @if($videoData->status == false) selected @endif>disabled</option>
		</select>
	</label>
</div>