@extends('layouts.admin')
@section('title') Video Manager @endsection

@section('content')

<div class="content-wrapper">
	<div class="row justify-content-center">
		<h3 class="my-1">Video Manager</h3>
	</div>
	<ul class="row menu-options justify-content-center">
		<li>
			<a href="/video-manager/create">
				<img src="" alt="">
				<h4>Add Video</h4>
			</a>
		</li>
		<li>
			<a href="/video-manager/all">
				<img src="" alt="">
				<h4>Edit Videos</h4>
			</a>
		</li>
	</ul>
	<div class="group">
		<div class="row justify-content-center">
			@if($status == 0)
			<h3 class="my-1">Videos That Need Attention</h3>
			@else 
			<h3 class="my-1">Videos On The Site</h3>
			@endif
		</div>
		<div class="row collection need-attention">
			@php  $i = 0; @endphp
			@foreach($videos as $video)
			@php 
				$videoData = new \stdClass(); $videoData->id = $video->id; 
				$video = $video->getContents();
			@endphp
				@include('admin.partials.video_preview')
				@php $i++; if($i == 4){$i = 0;} @endphp
			@endforeach
			@if($i == 1)
				@for($i = $i; $i < 4; $i++)
				<div class="video-preview m-1"></div>
				@endfor
			@endif
		</div>
		{!! $videos->render() !!}
	</div>
</div>

@endsection