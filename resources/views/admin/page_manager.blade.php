@extends('layouts.admin')
@section('title') Pages - Content Manager @endsection

@section('content')
<div class="content-wrapper">
	<div class="row justify-content-center">
		<h3 class="my-1">Page Manager</h3>
	</div>
	<ul class="row menu-options justify-content-center">
		<li>
			<a href="/content-manager/pages/create">
				<img src="" alt="">
				<h4>Add New Page</h4>
			</a>
		</li>
	</ul>
	<div class="collection row pages-container my-1">
		@foreach($pages as $page)
		<div class="page row no-wrap">
			<h3 class="page-title">{{ucwords(str_replace('-', ' ', $page->name))}}</h3>
			<a href="/content-manager/pages/{{$page->id}}" class="justify-right edit">Edit</a>
			<form action="/content-manager/pages/{{$page->id}}/delete" method="POST">
				<input class="not-an-input" type="submit" value="Delete">
				@include('partials.token')
			</form>
		</div>
		@endforeach
	</div>
</div>
@endsection