@extends('layouts.admin')
@section('title') Hello {{ucfirst(Auth::user()->name)}} @endsection

@section('content')
<div class="content-wrapper">
	<ul class="row option-bar justify-content-center">
		<li>
			<a href="/video-manager" class="row justify-content-center">
				<img src="{{ asset('images/icon-video-manager.png') }}" class="full" alt="">
				<h3>Video Manager</h3>
			</a>
		</li>
		<li>
			<a href="/content-manager" class="row justify-content-center">
				<img src="{{ asset('images/icon-content-manager.png') }}" class="full" alt="">
				<h3>Content Manager</h3>
			</a>
		</li>
		<li>
			<a href="/setting-manager" class="row justify-content-center">
				<img src="{{ asset('images/icon-setting-manager.png') }}" class="full" alt="">
				<h3>Setting Manager</h3>
			</a>
		</li>	
	</ul>
</div>
@endsection