@extends('layouts.admin')
@section('title') Persisting Elements - Content Manager @endsection

@php 
	$language = ['english', 'inuktitut', 'french']; 
	$navigation = array(0,1,2,3); 
	$home_page = array(4,5,6,7);
	$category = array(8,9);
	$collection = array($navigation, $home_page, $category);
	$text = array('Navigation Elements', 'Featuring Video Elements','Category Text Elements')
	
@endphp

@section('content')
<div class="content-wrapper">
	<div class="row justify-content-center">
		<h3 class="my-1">Persisting Elements</h3>
	</div>
	<form action="/content-manager/persistent" method="POST">
		<div class="form-block-container row">
			@for($i = 0; $i < 3; $i++)
			
			<div class="form-block">
				<h3 class="my-1">{{$text[$i]}}:</h3>
				@foreach($collection[$i] as $item)	
				<div class="form-group row no-wrap align-items-center">
					<h5 class="form-flex-fixed">{{ucwords($items[$item]->english)}}</h5>
					<div class="input-group row">
					@foreach($language as $lang)
						<div class="row">
							<label class="my-1" for="{{$items[$item]->id}}-{{$lang}}">{{ucwords($lang)}}:</label>
							<input type="text" name="{{$items[$item]->id}}[{{$lang}}]" id="{{$items[$item]->id}}-{{$lang}}" value="{{$items[$item]->$lang}}">
						</div>
					@endforeach
					</div>
				</div>
				@endforeach
			</div>
			@if($i+1 > count($item))
			@php break; @endphp
			@endif
			@endfor
		</div>
		<input class="p-1 m-1" type="submit" value="Update">
		@include('partials.token')
	</form>
</div>
@endsection