@if($page) @extends('layouts.admin') 
@section('title') Edit {{ucwords($page->field('page_title'))}} @endsection 
@section('content')
@if($page->contents()->count() == 2)

<form class="content-wrapper" action="/content-manager/pages/{{$page->id}}" method="POST">
	@include('admin.partials.tool_bar') @include('partials.token')
	<div class="row">
		<h2 class="my-1 editable" id="page_title">{!!ucwords($page->field('page_title'))!!}</h2>
	</div>
	<div class="row">
		<div id="toolbar"></div>
		<div class="editable editable-content my-1" id="page_content">
			{!! $page->field('page_content') !!}
		</div>

	</div>

	<input class="p-1" type="submit" value="Update">
</form>

@push('scripts')
<script src="{{ asset('/js/tinymce/tinymce.js') }}"></script>

<script type="text/javascript">
	if (!tinymce.init({
		selector: '.editable',
		inline: true,
		hidden_input: true,
		branding: false,
		fixed_toolbar_container: '#editor-toolbox',
		plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
	})) {

	}
</script> @endpush @else @endif @endsection @endif