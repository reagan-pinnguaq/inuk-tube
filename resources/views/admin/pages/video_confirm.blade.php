@extends('layouts.admin') 

@section('title') Comfirm the Video @endsection

@section('content')
<div class="content-wrapper">
	<h2 class="my-1">Confirm The Video</h2>
	<form action="/video-manager/confirm" method="POST" >
	 <label for="confirm"><input type="checkbox" name="confirm" id="confirm"> Yes, this is the video I expected.</label>
	 <input type="hidden" name="key" value="{{$video->key}}">
	 <input type="hidden" name="title" value="{{$video->title}}">
	 @include('partials.token')
	 <input type="submit" value="Confirm">
	</form>	
	@include('partials.video.video')
</div>
@endsection