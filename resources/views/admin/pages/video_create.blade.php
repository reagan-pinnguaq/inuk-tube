@extends('layouts.admin')
@section('title') Add A Video @endsection

@section('content')
	<div class="content-wrapper">
		<div class="row justify-content-center">
			<h2 class="my-1">Add A Video</h2>
		</div>
		<form action="/video-manager/" method="POST">
			@include('partials.token')
			<div class="form-group row">
				@include('partials.upload.source')
				@include('admin.partials.category_selector')
			</div>
			
				@include('admin.partials.tag_selector')
			
			<div class="form-group row">
				<input type="submit" value="Add Video" class="p-1">
			</div>
		</form>
	</div>
@endsection