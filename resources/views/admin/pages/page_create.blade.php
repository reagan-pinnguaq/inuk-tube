@extends('layouts.admin')
@section('title') Persisting Elements - Content Manager @endsection

@section('content')
<div class="content-wrapper">
	<div class="row justify-content-center py-2">
		<h3 class="my-3">Create A New Page</h3>
	</div>
	<form action="/content-manager/pages/" method="POST">
	<h2>
		<input type="text" class="inline" name="title" id="" required placeholder="Enter the Page Title">
	</h2>
	
	<p class="m-1"><textarea name="content"></textarea></p>
	
	<p><input type="submit" value="create" class="pt-1 pb-1 pl-2 pr-2 m-1"></p>
	@include('partials.token')
	</form>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>
	tinymce.init({
		selector: 'textarea',
		browser_spellcheck: true,
		menubar:false,
    statusbar: false,
		toolbar: 'bold, italic, underline, strikethrough, alignleft, aligncenter, alignright, alignjustify, styleselect, formatselect, fontselect, fontsizeselect, cut, copy, paste, bullist, numlist, outdent, indent, blockquote, undo, redo, removeformat, subscript, superscript'
	});
</script>
@endpush

