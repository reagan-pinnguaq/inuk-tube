@extends('layouts.admin')
@section('title') Edit Video @endsection

@section('content')
	<div class="content-wrapper">
			
		<div class="row justify-content-center">
			<h2 class="my-1">Edit Video</h2>
		</div>
		<ul class="row sub-nav justify-content-center">
			@if($videoData->previous() !== null)
			<li classs="justiify-left"><a href="/video-manager/{{$videoData->previous()->id}}">Previous Video</a></li>
			@endif
			<li @if($videoData->previous() !== null || $videoData->next() !== null) class="justify-right" @endif ><a href="@if($videoData->status == 1) /video-manager/all @else /video-manager @endif">Back to Listing</a></li>
			@if($videoData->next() !== null)
			<li class="justify-right"><a href="/video-manager/{{$videoData->next()->id}}">Next Video</a></li>
			@endif
		</ul>

		<form action="/video-manager/{{$videoData->id}}" method="POST">
			<input type="hidden" name="stored[url]" 
			@if(isset($videoData->key))
					{{'value=' . $videoData->key}}
			@endif
			>
			<input type="hidden" name="stored[video_type]" value="{{$videoData->type_id}}">
			<input type="hidden" name="stored[status]" value="{{$videoData->status}}">
			<div class="row">
				<div class="two-third">
					<h2 class="text-center my-1">Preview of the Current Video</h2>
					@php $video = $videoData->getContents(); @endphp
					@include('partials.video.video')
				</div>
				<div class="form-group column video-info">
					@include('partials.upload.source')
					@include('admin.partials.video_status')
					
				</div>
			</div>
				@include('admin.partials.category_selector')
				@include('admin.partials.tag_selector')
			
			<div class="form-group row">
				@include('partials.token')
				<input type="submit" value="Update Video" class="p-1">
			</div>

		</form>
		
		<form action="/video-manager/{{$videoData->id}}" method="POST">
			<input type="submit" value="Remove Video" class="p-1">		
			@php $token = csrf_token();@endphp
			<input type="hidden" name="_token" value="{{$token}}">
			<input type="hidden" name="{{$token}}" value="DELETE">
		</form>
	</div>
@endsection