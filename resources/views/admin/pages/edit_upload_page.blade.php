@extends('layouts.admin') 

@section('title') Edit {!! ucwords(\App\LangContent::where('field', 'LangAddVideoTitle')->first()->$lang) !!} @endsection
@section('description')  @endsection

@section('content')

<div class="content-wrapper">
	@include('admin.partials.tool_bar')
	<form action="/content-manager/pages/{{$page[0]->page_id}}" method="POST" >
	<h2 class="my-1 editable" id="LangAddVideoTitle">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoTitle')->first()->$lang) !!}</h2>
		@include('partials.message')
	<div class="row">
		<div class="half">
		@include('partials.upload.explain')
		@include('partials.upload.source')
		@include('partials.upload.validator')
		</div>
		<div class="half">
		@include('partials.upload.policy')
		</div>
	</div>
	 
	 <input class="p-1" type="submit" id="submit" value="{!! ucwords(\App\LangContent::where('field', 'LangAddVideoUploadButton')->first()->$lang) !!}">
	</form>
	
</div>
@endsection

@push('scripts')
<script>
//setup existing template for backend
var toggles = document.querySelectorAll("[required]");
for(i=0; i < toggles.length; i++)
{	toggles[i].removeAttribute('required'); toggles[i].removeAttribute('name'); }
var labels = document.querySelectorAll("[for]");
for(i=0; i < labels.length; i++)
{	labels[i].removeAttribute('for');}
button = document.getElementById('submit').value = 'Update Page';
</script>
<script src="{{ asset('/js/tinymce/tinymce.js') }}"></script>
<script type="text/javascript">
if(!tinymce.init({
	selector:'.editable',
	inline:true,
	hidden_input:true,
	branding: false,
	fixed_toolbar_container: '#editor-toolbox',
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
})){
	
}
</script>
@endpush