@extends('layouts.admin')
@section('title') Setting Manager @endsection
@section('content')
<div class="content-wrapper">
	<div class="row justify-content-center">
		<h3 class="my-1">Setting Manager</h3>
	</div>
	<form action="/setting-manager/" method="POST" >
		<div class="row collection">
		@foreach($settings as $setting)
			<div class="form-group half my-1">
				<div class="row no-wrap">
					<label for="{{$setting->field}}">{!! ucwords(str_replace('_', ' ', $setting->field)) !!}: </label>
					@include('admin.partials.setting_changer')
				</div>
				<p>
					{{$setting->description}}
				</p>
			</div>	
		@endforeach
		</div>
	@include('partials.token')
	<input type="submit" class="p-1" value="Save Settings">
	</form>
</div>
@endsection