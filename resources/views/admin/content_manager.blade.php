@extends('layouts.admin')
@section('title') Content Manager @endsection

@section('content')

<div class="content-wrapper">
	<div class="row justify-content-center">
		<h3 class="my-1">Content Manager</h3>
	</div>
	<ul class="row menu-options justify-content-center">
		<li>
			<a href="/content-manager/pages">
				<img src="" alt="">
				<h4>Manage Pages</h4>
			</a>
		</li>
		<li>
			<a href="/content-manager/persistent">
				<img src="" alt="">
				<h4>Manage Persisting Elements</h4>
			</a>
		</li>
	</ul>
</div>

	@endsection
