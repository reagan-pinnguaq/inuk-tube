@extends('layouts.master') 

@section('title') Categories @endsection
@section('description') A list of categories and preview to the types of videos in each category. @endsection

@section('content')
	<div class="content-wrapper">
	@if(count($archive) > 1 )
		<?php $videoLimit = \App\Settings::where('field', 'category_limit')->first()->value; $multi = true;?>
		@foreach($archive as $category)
			@include('partials.archive.category')
		@endforeach

	@else 
	
		<?php $category = $archive; ?>
		@include('partials.archive.category')

	@endif
	</div>
@endsection