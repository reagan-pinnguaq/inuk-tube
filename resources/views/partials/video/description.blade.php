<div class="video-description-container">
	@if(strlen($video->description) <= \App\Settings::where('field', 'video_description_length')->first()->value) 
	<div class="video-description description-short">
		<p>{!! str_replace("\n", '</p><p>', $video->description) !!}</p>
	</div>
	@else
	<div class="video-description description-short">
		<p>{!! \App\Model::limitText(str_replace("\n", '</p><p>', $video->description), \App\Settings::where('field', 'video_description_length')->first()->value) . '</p>' !!}</p>
	</div>
	<div class="video-description description-full" style="display:none">
		<p>{!! str_replace("\n", '</p><p>', $video->description) !!}</p>
	</div>
	<div class="video-description-toggle">
		<h5 class="pointer">Show More</h5>
	</div>
	@endif
</div>

@push('scripts')
<script src="{{ asset('/js/description.js')}}"></script>
@endpush