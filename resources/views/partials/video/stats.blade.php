<ul class="video-stats">
	<li class="row justify-content-right">
		<h5 class="text-upper">Share</h5>
		<ul class="row shares no-wrap">
			<li class="one-third"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//inuk-tube.com/{{$video->key}}"><img src="{{ asset('images/icon-facebook.png') }}" alt="" srcset=""></a></li>
			<li class="one-third"><a href="https://twitter.com/home?status=Shared%20a%20video%20from%20inuktitube%3A%20http%3A//inuk-tube.com/{{$video->key}}"><img src="{{ asset('images/icon-twitter.png') }}" alt="" srcset=""></a></li>
			<li class="one-third"><a href="https://plus.google.com/share?url=http%3A//inuk-tube.local/{{$video->key}}"><img src="{{ asset('images/icon-google.png') }}" alt="" srcset=""></a></li>
		</ul>
	</li>
	<li class="row"><img width="30px" src="{{ asset('images/icon-views.png') }}" alt=""><span class="video-external-count">{{$video->externalViews}}</span></li>
	<li class="row no-wrap">
		<img style="flex:0 0 30px;" src="{{ asset('images/icon-tag.png') }}" alt="">
		<div class="video-tags-container row">
			@foreach($video->tags as $tag)
				<div class="video-tag">{{$tag->tag()->$lang}}</div>
			@endforeach
		</div>
	</li>
</ul>