@if(!empty($related))
<div id="related" class="py-1">
	<div class="content-wrapper">
		<h2 class="my-1 text-center">{{ucwords($related_title->$lang)}}</h2>
		<div class="related-items row collection my-1">
			@foreach($related as $video)
				@php $video = $video->getContents(); @endphp
				@include('partials.video.preview')
			@endforeach
		</div>
	</div>
</div>
@endif