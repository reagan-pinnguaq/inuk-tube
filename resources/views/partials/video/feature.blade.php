<div id="featured" class="py-1">
	<div class="content-wrapper">
		<div class="row justify-content-center">
			<h2 class="text-center my-1">{{ $feature_title->$lang }}</h2>
		</div>
	</div>
	<div class="feature-container">
		@foreach($features as $video)
		@php $video = $video->video()->getContents(); @endphp
				<a href="/{{$video->key}}" class="feature-item pointer row align-items-end" style="background-image:url('{{$video->image}}');">
					<h3 class="feature-item-title">{!! ucwords(\App\Model::limitText($video->title, 40)) !!}</h3>
				</a>
		@endforeach
	</div>
</div>

@push('styles')
<link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet" type="text/css" >
@endpush

@push('scripts')
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('/js/slick.min.js') }}"></script>
<script>
	$(document).ready(function(){
		$('.feature-container').slick({
			arrows:false,
			slidesToShow: 3,
			centerMode: true,
			dots: true,
			responsive: [
				{
					breakpoint: 750,
					dots: true,
					settings: {
						arrows: false,
						slidesToShow: 1
					}
				}
			]
		});
		$
});
</script>
@endpush
