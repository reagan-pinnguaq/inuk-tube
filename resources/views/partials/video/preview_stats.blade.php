<div class="preview-video-stats full align-self-end row p-0 mx-0">

	<span class="views ">
	@if($video->externalViews < 1000)
		{{$video->externalViews}}
	@elseif($video->externalViews < 1000000)
		{!! round($video->externalViews / 1000, 1) . 'K' !!} 
	@else
		{!! round($video->externalViews / 1000000, 1) . 'M' !!} 
@endif
	  {!! ucwords($view->$lang) !!}</span>

	<span class="dot">&#8226;</span>
	@if ($lang == 'french')
		@php \Carbon\Carbon::setLocale('French'); @endphp
	@elseif($lang == 'inuktitut')
		@php \Carbon\Carbon::setLocale('Inuktitut'); @endphp
	@endif
	<span class="date">{!! \Carbon\Carbon::parse($video->createdOn)->diffForHumans() !!}</span>
</div>