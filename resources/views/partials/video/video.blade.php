<div class="video content-wrapper my-1">
	@include('partials.video.embed')
	<div class="video-fold row">
		<div class="video-info row">
			<div class="two-third my-1">
				@include('partials.video.title_external')
				@include('partials.video.description')
			</div>
			<div class="one-quarter">
				@include('partials.video.stats')
			</div>
		</div>
	</div>
</div>