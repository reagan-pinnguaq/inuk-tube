<?php if(isset($video->key)):?>

<div id="{{$video->key}}" class="video-preview row m-1">
	@include('partials.video.image_thumbnail')
	@include('partials.video.title')
	@include('partials.video.preview_stats')
</div>

<?php endif;?>