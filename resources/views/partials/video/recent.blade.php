<div id="recent" class="py-1">
	<div class="content-wrapper">
		<div class="row justify-content-center">
			<h2 class="text-center my-1">{{ $recent_title->$lang }}</h2>
		</div>
	</div>
	<div class="content-wrapper">
	<!-- <button class="prev slick-prev">prev</button> -->
		<div class="recent-container">
		
			@foreach($recents as $video)
			
			@php $video = $video->getContents(); @endphp
				<div class="recent-item row">
					
					@include('partials.video.image_thumbnail')
					@include('partials.video.title')
					@include('partials.video.tags')
					
				</div>
			@endforeach
			
		</div>
		<!-- <button class="next slick-next slick-arrow">next</button> -->
	</div>
</div>

@push('styles')
<link href="{{ asset('css/slick.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet" type="text/css" >
@endpush

@push('scripts')
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="{{ asset('/js/slick.min.js') }}"></script>
<script>
	$(document).ready(function(){
		$('.recent-container').slick({
			// arrows:true,
			slidesToShow: 3,
			responsive: [
				{
					breakpoint: 750,
					settings: {
						// arrows: true,
						slidesToShow: 1
					}
				}
			]
		});
});
</script>
@endpush
