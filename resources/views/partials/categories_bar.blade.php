<div class="content-wrapper categories-bar py-1">
	<div class="row justify-content-center">
			<h2 class="text-center my-1"><a href="/{{\App\Model::slugify($category_title->$lang)}}">{!! $category_title->$lang !!}</a></h2>
	</div>

	<ul class=" row justify-content-center py-1">
		@foreach ($categories as $category)
			<li class="text-center m-1"><a href="/{!! \App\Model::slugify($category_title->$lang) !!}\{!! \App\Model::slugify($category->$lang) !!}">{!! $category->$lang !!}</a></li>
		@endforeach
	</ul>
</div>