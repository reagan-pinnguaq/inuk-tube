<?php $z = rand(1,25); $y = rand(1,25); $x = array('*', '+', '-'); $x = $x[array_rand($x)]; ?>
<div class="form-group">
	<h5 class="my-1"><label for="" id="LangAddVideoVerifyTitle" class="editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoVerifyTitle')->first()->$lang) !!}</label></h5>
	<p>{{$z}} {{$x}} {{$y}} = <input type="number" name="uAnswer" placeholder="{!! ucwords(\App\LangContent::where('field', 'LangAddVideoVerifyPlaceholder')->first()->$lang) !!}" required></hp>
	<input type="hidden" name="answer" value="{!! \App\Model::calculate_string($z .  $x . $y) !!}">
	{{ csrf_field() }}
</div>