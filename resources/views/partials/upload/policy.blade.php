<div class="form-group">
	<div class="upload-tos">
		<h5 id="LangAddVideoAgreeTitle" class="my-1 editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoAgreeTitle')->first()->$lang) !!}</h5>
		<p id="LangAddVideoAgreeBody" class="editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoAgreeBody')->first()->$lang) !!}</p>
		
		<h5 id="LangAddVideoTermTitle" class="my-1 editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoTermTitle')->first()->$lang) !!}</h5>
		<p id="LangAddVideoTermBody" class="editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoTermBody')->first()->$lang) !!}</p>
	</div>
	<input type="checkbox" name="accept" required><label for="accpet" id="LangAddVideoConfirmTitle" class="editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoConfirmTitle')->first()->$lang) !!}</label>
</div>