<div class="form-group">
	<h5 class="my-1"><label for="url" id="LangAddVideoLinkTitle" class="editable">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoLinkTitle')->first()->$lang) !!}</label></h5>
	<input type="url" id="url" name="url" class="pl-1" required 
	@if(isset($videoData->key))
		@if($videoData->type_id == 0)
			{{'value=https://vimeo.com/' . $videoData->key }}
		@else
			{{'value=https://www.youtube.com/watch?v=' . $videoData->key}}
		@endif
	@endif
	>
</div>
<div class="form-group">
	<h5 class="my-1"><label for="source" class="editable" id="LangAddVideoHostedTitle">{!! ucwords(\App\LangContent::where('field', 'LangAddVideoHostedTitle')->first()->$lang) !!}</label></h5>
	<select name="video_type" id="source" class="pl-1" required>
		<option value="" disabled selected></option>
		@foreach($types as $type)
			<option value="{{$type->id}}" 
			@if(isset($videoData->type_id) && $videoData->type_id == $type->id) selected @endif>
			{!! ucwords($type->name) !!}</option>
		@endforeach
	</select>
</div>