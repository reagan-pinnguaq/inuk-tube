<nav id="main-nav" class="row">
	<ul class="nav row justify-content-start align-items-center content-wrapper">
		<input type="checkbox" name="menu-state" id="menu-state" checked>
		<li>
			<form action="/search/" method="GET">
				<input type="search" name="query" placeholder="{{ strtoupper($nav['search']->$lang) }}">
			</form>
		</li>
		<li><a class="text-upper" href="/{!! \App\Model::slugify($nav['add']->$lang) !!}">{{ $nav['add']->$lang }}</a></li>
		<li><a class="text-upper" href="/{!! \App\Model::slugify($nav['category']->$lang) !!}">{{ $nav['category']->$lang }}</a></li>
		<li><a class="text-upper" href="/videos/{!! \App\Model::slugify($nav['random']->$lang) !!}">{{ $nav['random']->$lang }}</a></li>
		<li class="justify-right">
			<ul class="row mb-0">
				<li><a class="text-upper" href="/language/english">en</a></li>
				<!-- <li><a class="text-upper" href="/language/french">fr</a></li> -->
				<li><a class="text-upper" href="/language/inuktitut">in</a></li>
			</ul>
		</li>
		@if (Auth::user())
		<li><a class="text-upper" href="/dashboard">Dashboard</a></li>
		<li>@include('auth.logout')</li>
		@else
		
		@endif
		<li class="menu-icon">
			<label for="menu-state" class=" my-1">
				<span></span>
				<span></span>
				<span></span>
			</label>
		</li>
	</ul>
</nav>