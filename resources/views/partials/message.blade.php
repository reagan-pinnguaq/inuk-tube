@if(Session::has('message'))
<div class="alert alert-message">
	<p>{{ Session::get('message') }}</p>
</div>
@endif