<div class="category my-2">
	<h2 class="category-title my-1"><a href="/categories/{!! App\Model::slugify($category->title(session('lang'))) !!}">{{$category->title(session('lang'))}}</a></h2>
	<div class="row collection">

	<?php if(!isset($videoLimit)): $videoLimit = null; endif; $i=0;?>
		@foreach($category->videos($videoLimit) as $videoCategory)
			@if($videoCategory->video()->status == true)
				@php $video = $videoCategory->video()->getContents(); @endphp
				@include('partials.video.preview')
				@php $i++; if($i == 4){$i = 0;} @endphp
			@endif
		@endforeach
		@if($i == 1)
			@for($i = $i; $i < 4; $i++)
			<div class="video-preview m-1"></div>
			@endfor
		@endif
	</div>
	@if(isset($multi) && $multi == true)
	<div class="category-director m-1">
		<a class="mx-1" href="/categories/{!! App\Model::slugify($category->english) !!}">{!! ucwords($more->$lang) !!}</a>
	</div>
	@else 
	<div class="paginator">{!! $category->videos()->render() !!}</div>
	@endif
</div>