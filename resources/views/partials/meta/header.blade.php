<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="title" content="@yield('title') - Inukitube">
		<meta name="description" content="@yield('description')">
    <title>@yield('title') - Inukitube</title>

    <!-- <link rel="icon" href="{{ asset('images/favicon.ico')}}"> -->
    
    <!-- Stack styles -->
    @stack('styles')
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
</head>
<body class="@yield('title')">