<div class="video_list">
	<h4>{{$result->total()}} videos with {{$term}}:</h4>
	<div class="row collection">
	@php $i = 0; @endphp
	@foreach($result as $item)
		@include('partials.search.list')
		@php $i++; if($i == 4){$i = 0;} @endphp
	@endforeach
	@if($i == 1)
		@for($i = $i; $i < 4; $i++)
			<div class="video-preview m-1"></div>
		@endfor
	@endif
	</div>
	{!! $result->render() !!}
</div>