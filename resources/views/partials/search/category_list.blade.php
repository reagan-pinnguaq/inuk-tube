@if($categories !== null)
	@foreach($categories as $category)

		<h5><a href="" class="keyword">{{$category->title(session('lang'))}}</a> is also a category:</h5>
		
		<div class="row collection">

			@foreach($category->videos(4) as $videoCategory)
				@php $video = $videoCategory->video()->getContents() @endphp
				@include('partials.video.preview')
			@endforeach
		
		</div>
		<div class="category-director m-1">
			<a class="mx-1" href="/categories/{!! App\Model::slugify($category->title(session('lang'))) !!}">View More</a>
		</div>
	@endforeach
@endif