<div id="{{$item->key}}" class="video-preview row m-1">
<?php $video = $item->getContents();?>
	@include('partials.video.image_thumbnail')
	<h4 class="my-1"><a href="/{{$video->key}}">{!! \App\Model::matchAndHighlight($term, ucwords($item->name)) !!}</a></h4>
	@include('partials.video.preview_stats')
</div>