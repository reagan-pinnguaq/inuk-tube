let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
			
		.sass('resources/assets/sass/app.scss', 'public/css')
		.sass('resources/assets/sass/admin.scss', 'public/css')
		// .copy('node_modules/slick-carousel/slick/slick.css', 'public/css')
		// .copy('node_modules/slick-carousel/slick/slick-theme.css', 'public/css')
		// .copy('node_modules/slick-carousel/slick/slick.min.js', 'public/js')
		// .copy('resources/assets/js/description.js', 'public/js')
		// .copy('vendor/tinymce/', 'public/js', false)
		// .copy('resources/assets/fonts/', 'public/fonts', false)
		