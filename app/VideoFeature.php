<?php

namespace App;

use App\Model;
use Carbon\Carbon;

class VideoFeature extends Model
{   
    function video(){
        return $this->belongsTo(Video::class)->first();
    }

    protected $fillable = ['video_id'];

    public static function discernContent(){
        $self = new self;
        $dt = new Carbon($self::first()->created_at);
        if($dt->isPast() && !$dt->isToday()){
            $self->degenerate();
            $self->generate();
        }
        return self::all();
    }

    public static function seedVideoFeature(){
        $self = new self;
        if($self::all()->count() == 0){
            $self->generate();
        }
    }

    public static function isUnique($id){
        return (self::where('video_id', $id)->first() === null ? true : false);
    }

    private function degenerate(){
        self::truncate();
    }

    private function generate(){
        $limit = \App\Settings::where('field', 'feature_count')->first()->value;
        //Add entries based on limit
        for($i = $limit; $i > 0; $i--){
            $self = new self;
            $id = \App\Video::inRandomOrder()->first()->id;
            //Make sure id is unique
            while($self::isUnique($id) === false){
                $id = \App\Video::inRandomOrder()->first()->id;
            }
            $self->video_id = $id;
            $self->save();
        }   
    }

}
