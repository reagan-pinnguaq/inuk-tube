<?php

namespace App;

use App\Model;
use Laravel\Scout\Searchable;

class Category extends Model
{     
    use Searchable;
    
    public function toSearchableArray()
    {
        return ['english' => $this->english, 'french' => $this->french, 'inuktitut' => $this->inuktitut ];
    }

    function children(){
        return $this->hasMany(SubCategory::class);
    }

    function videos($limit = null){
        if($limit === null){$limit = \App\Settings::where('field', 'paginator')->first()->value;}
        return $this->hasMany(VideoCategory::class)
        ->where('category_id', $this->id)
        ->orderBy('updated_at', 'desc')
        ->paginate($limit);
    }

    public function title($lang){
        return $this->$lang;
    }

    public static function isCategory($str){
        $categories = Category::all();
        foreach($categories as $cat){
            if($str == parent::slugify($cat->english) || $str == parent::slugify($cat->french) || $str == parent::slugify($cat->inuktitut)){
                return true;
            }
        }
        return false;
    }

    public static function categoryID($str){
        $categories = Category::all();
        foreach($categories as $cat){
            if($str == parent::slugify($cat->english) || $str == parent::slugify($cat->french) || $str == parent::slugify($cat->inuktitut)){
                return $cat->id;
            }
        }
    }

    public static function categories(){
        return static::all();
    }
    

}
