<?php

namespace App;
use App\Model;
class VideoCategory extends Model
{
    function category(){
        return $this->belongsTo(Category::class);
    }

    function video(){
        return $this->belongsTo(Video::class)->first();
    }
}
