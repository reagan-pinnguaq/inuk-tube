<?php
namespace App\Rules;
use Illuminate\Contracts\Validation\Rule;
class Video implements Rule
{
    /**
     * The source control provider instance.
     *
     * @var \App\Source
     */
    protected $key;
    /**
     * The branch name.
     *
     * @var string
     */
    public $url;

    public()
    /**
     * Create a new rule instance.
     *
     * @param  \App\Source  $source
     * @param  string  $branch
     * @return void
     */
    public function __construct($source, $branch)
    {
        $this->source = $source;
        $this->branch = $branch;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (! $this->source instanceof Source) {
            return false;
        }
        return $this->source->client()->validRepository(
            $value, $this->branch
        );
    }
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The given repository is invalid.';
    }
}