<?php

namespace App;
use App\Model;

class Page extends Model
{
    protected $fillable = ['name']; 
    
    function contents(){
        return $this->hasMany(LangContent::class)->get();
    }

    /**
     * function getField
     *
     * @param [string] $field 'content->field'
     * @param boolean $id defaults:false
     * @return string 'field->value' : integer 'field->id'
     *
     */
    function field($field){
        $lang = session('lang');
        $content = $this->contents();
        foreach($content as $con)
        {
            if($con->field == $field){
                return $con->$lang;
            }
        }
    }
}
