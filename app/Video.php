<?php

namespace App;

use App\Model;
use Alaouy\Youtube\Facades\Youtube;
use Vinkla\Vimeo\Facades\Vimeo;

use Laravel\Scout\Searchable;

class Video extends Model
{

    use Searchable;

    public function toSearchableArray()
    {
        return ['name' => $this->name];
    }

    protected $fillable = ['name', 'type_id', 'views', 'status', 'aprover_id'];



    function categories()
    {
        return $this->hasMany(VideoCategory::class)->get();
    }

    function tags()
    {
        return $this->hasMany(VideoTag::class)->get();
    }

    function getRelatedVideos()
    {
        $tags = $this->tags();
        $collection = array();
        $related = array();
        foreach ($tags as $tag) {
            $results = \App\VideoTag::where('tag_id', $tag->tag_id)->get();
            if (!empty($results)) {
                foreach ($results as $result) {
                    $collection[] = $result->video()->id;
                }
            }
        }
        if (!empty($collection)) {
            $ranked = array_keys(array_count_values($collection));
            $counter = 0;
            $limiter = 4;
            foreach ($ranked as $item) {
                if ($item != $this->id) {
                    $related[] = Video::find($item);
                    $counter++;
                    if ($counter >= $limiter) {
                        break;
                    }
                }
            }
        }
        return $related;
    }

    function getContents()
    {
        switch ($this->type_id) {
            case 1: // youtube
                return $this->buildYoutube(Youtube::getVideoInfo($this->key));
                break;
            case 2: // vimeo 
                return $this->buildVimeo(Vimeo::request('/videos/' . $this->key));
                break;
            default:
                return false;
        }
    }
    public function getVideoKey($url, $video_id)
    {
        switch ($video_id) {
            case 1:
                $key = $this->getYoutubeKey($url);
                break;
            case 2:
                $key = $this->getVimeoKey($url);
                break;
            default:
                $key = false;
                break;
        }
        return $key;
    }

    function next()
    {
        return Video::where('status', $this->status)
            ->where('id', '>', $this->id)
            ->orderBy('id', 'asc')
            ->first();
    }
    function previous()
    {
        return Video::where('status', $this->status)
            ->where('id', '<', $this->id)
            ->orderBy('id', 'dsc')
            ->first();
    }

    public static function getYoutubeKey($url)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $e);
        if ($e != null) {
            return $e['v'];
        }
        return false;
    }

    public static function getVimeoKey($url)
    {
        $regs = array();
        //(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*
        if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $url, $regs)) {
            $id = $regs[3];
            return $id;
        }
        return false;
    }


    /**
     * Undocumented function
     *
     * @param {json object} $data
     * @return object
     */
    private function buildYoutube($data)
    {

        $arr =  new \stdClass();
        $arr->key           = $this->key;
        $arr->title         = $data->snippet->title;
        $arr->description   = $data->snippet->description;
        $arr->link          = 'https://www.youtube.com/watch?v=' . $data->id;
        $arr->internalViews = $this->views;
        $arr->externalViews = $data->statistics->viewCount;
        $arr->releasedOn    = $data->snippet->publishedAt;
        $arr->createdOn     = $this->created_at;
        if (isset($data->statistics->likeCount)) {
            $arr->likes         = $data->statistics->likeCount;
        } else {
            $arr->likes = 0;
        }
        $arr->embed         = 'https://www.youtube.com/embed/' . $this->key;
        $arr->thumbnail     = $data->snippet->thumbnails->medium->url;
        $arr->image         = $data->snippet->thumbnails->high->url;
        $arr->tags          = $this->tags();
        return $arr;
    }
    /**
     * Undocumented function
     *
     * @param jsonobject{} $data
     * @return object 
     */
    private function buildVimeo($data)
    {
        $arr =  new \stdClass();
        $arr->key           = $this->key;
        $arr->title         = $data['body']['name'];
        $arr->description   = $data['body']['description'];
        $arr->link          = $data['body']['link'];
        $arr->internalViews = $this->views;
        $arr->externalViews = $data['body']['stats']['plays'];
        $arr->releasedOn    = $data['body']['release_time'];
        $arr->likes         = $data['body']['metadata']['connections']['likes']['total'];
        $arr->embed         = 'https://player.vimeo.com/video/' . $this->key;
        $arr->thumbnail     = $data['body']['pictures']['sizes']['2']['link'];
        $arr->image         = $this->vimeoHQimage($data['body']['pictures']['sizes']);
        $arr->tags          = $this->tags();
        return $arr;
    }

    private function viemoHQimage($data)
    {
        $youtubeSize = 480; //to match youtubes high res output.
        $maxWidth = 650; // cutt off size.
        $counter = 1; // our index. Set to one to force the next size above lowest under youtube size

        foreach ($data as $item) {
            if ($item['width'] < $youtubeSize) {
                $counter++;
            }
        }

        if ($data[$counter]['width'] > $maxWidth) {
            $counter--;
        }

        return $data[$counter]['link'];
    }

    public function revalidateYoutube()
    {
        if ($this->type_id !== 1) {
            return $this;
        }

        $data = $this->getContents();
        $this->name = $data['title'];
        $this->save();
        return $this;
    }
}
