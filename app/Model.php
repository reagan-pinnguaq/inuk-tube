<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    public static function slugify($str) {
        $str = strtolower(trim($str));
        $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);
        return preg_replace('/\-{2,}/', '-', $str);
    }

    public static function limitText($x, $length){
        if(strlen($x) <= $length) { 
            return $x;
        } else {
            return preg_replace('/\s+?(\S+)?$/', '', substr($x, 0, $length+1)) . ' ...';
        }
    }

    public static function calculate_string($mathString)    {
        $mathString = trim($mathString);     // trim white spaces
        $mathString = preg_replace ('[^0-9\+-\*\/\(\) ]', '', $mathString);    // remove any non-numbers chars; exception for math operators
        $compute = create_function("", "return (" . $mathString . ");" );
        return 0 + $compute();
    }

    public static function matchAndHighlight($param, $string){
        $param = explode(' ', ucwords($param));
        foreach ($param as $word) {
           $matchWords[] = "/$word/i";
        }
        $string = preg_replace($matchWords, '<span class="keyword">$0</span>', $string);
        return $string;
    }
}