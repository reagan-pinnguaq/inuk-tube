<?php

namespace App;
use App\Model;
class SubCategory extends Model
{
    
    function title($lang){
        return $this->$lang;
    }

	function parent(){
			return $this->belongsTo(Category::class);
    }
    
    function videos(){
      return $this->hasMany(VideoSubCategory::class)->where('sub_category_id', $this->id)->get();
  }

    public static function isCategory($str){
        $categories = SubCategory::all();
        foreach($categories as $cat){
            if($str == parent::slugify($cat->english) || $str == parent::slugify($cat->french) || $str == parent::slugify($cat->inuktitut)){
                return true;
            }
        }
        return false;
    }
    
    public static function subCategoryID($str){
        $categories = SubCategory::all();
        foreach($categories as $cat){
            if($str == parent::slugify($cat->english) || $str == parent::slugify($cat->french) || $str == parent::slugify($cat->inuktitut)){
                return $cat->id;
            }
        }
    }
}