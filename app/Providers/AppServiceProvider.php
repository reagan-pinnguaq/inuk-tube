<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        //For databases
        Schema::defaultStringLength(191);

        // manage sessions
        if(session('lang') == null) { session(['lang' => 'english']);}

        /***************************
         *  Front End Composers
         **************************/ 

        view()->composer('partials.page.nav', function($view){
            $view->with('nav', array(
                'search' => \App\LangContent::where('field', 'LangNavSearch')->first(),
                'add' => \App\LangContent::where('field', 'LangNavAddVideo')->first(),
                'category' => \App\LangContent::where('field', 'LangNavCategories')->first(),
                'random' => \App\LangContent::where('field', 'LangNavRandomVideo')->first()
            ))->with('lang', session('lang'));
        });

        view()->composer('partials.upload.category_selector', function($view){
            $view->with('categories', \App\Category::all());
        });

        view()->composer('partials.upload.source', function($view){
            $view->with('types', \App\VideoType::all());
        });

        view()->composer('partials.archive.category', function($view){
            $view->with('more', \App\LangContent::where('field', 'LangCategoryViewMore')->first())
                 ->with('lang', session('lang'));
        });

        view()->composer('partials.video.preview_stats', function($view){
            $view->with('view', \App\LangContent::where('field', 'LangPreviewViews')->first())
                 ->with('lang', session('lang'));
        });
        view()->composer('partials.video.stats', function($view){
            $view->with('lang', session('lang'));
        });
        
        view()->composer('partials.video.feature', function($view){
            $view->with('features', \App\VideoFeature::discernContent())
                 ->with('lang', session('lang'))
                 ->with('feature_title', \App\LangContent::where('field', 'LangFeaturedTitle')->first());
        });

        
        view()->composer('partials.video.recent', function($view){
            $view->with('recents', \App\Video::where('status', 1)
                 ->orderBy('created_at', 'desc')
                 ->limit(\App\Settings::where('field', 'recent_count')->first()->value)
                 ->get()
                 )
                 ->with('lang', session('lang'))
                 ->with('recent_title', \App\LangContent::where('field', 'LangRecentTitle')->first());
        });

        view()->composer('partials.video.related', function($view){
            $view->with('related_title', \App\LangContent::where('field', 'LangRelatedTitle')->first())
                 ->with('lang', session('lang'));
        });

        view()->composer('partials.categories_bar', function($view){
            $view->with('categories', \App\Category::all())
                 ->with('lang', session('lang'))
                 ->with('category_title', \App\LangContent::where('field', 'LangCatBarTitle')->first());
        });

        view()->composer('page.video_confirm', function($view){
            $view->with('lang', session('lang'))
                 ->with('title', \App\LangContent::where('field', 'LangConfirmVideoTitle')->first())
                 ->with('label', \App\LangContent::where('field', 'LangConfirmVideoLabel')->first())
                 ->with('button', \App\LangContent::where('field', 'LangConfirmVideoButton')->first());
        });

        view()->composer('partials.upload.source', function($view){
            $view->with('lang', session('lang'));
        });
        
        


        /***************************
         *  Back End Composers
         **************************/ 

        view()->composer('admin.partials.category_selector', function($view){
            $view->with('categories', \App\Category::all());
        });
        view()->composer('admin.partials.tag_selector', function ($view){
            $view->with('tags', \App\Tag::all());
        });

        view()->composer('admin.partials.nav', function($view){
            $view->with('verify_count', count(\App\Video::where('status', 0)->get()));
        });

        view()->composer('admin.pages.edit_upload_page', function($view){
            $view->with('lang', session('lang'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
