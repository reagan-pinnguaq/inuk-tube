<?php

namespace App;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{   
    use Searchable;

    public function toSearchableArray(){
        return ['name' => $this->name];        
    }

    protected $fillable = ['name'];
    function videos()
    {
        if($limit === null){$limit = \App\Settings::where('field', 'paginator')->first()->value;}
        return $this->hasMany(VideoTag::class)
        ->where('tag_id', $this->id)
        ->orderBy('created_at', 'desc')
        ->paginate($limit);
    }

}
