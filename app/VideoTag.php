<?php

namespace App;
use App\Model;

class VideoTag extends Model
{
    function tag(){
        return $this->belongsTo(Tag::class)->first();
    }

    function video(){
        return $this->belongsTo(Video::class)->first();
    }
    function test(){
        return 'hey';
    }
}
