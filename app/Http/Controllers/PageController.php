<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show($page)
    {   

        // dd($page);
        $page = Page::where('name', $page)->first();
        return view('page.simple', compact('page'));
    }
}
