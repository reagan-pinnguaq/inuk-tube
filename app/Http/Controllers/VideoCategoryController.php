<?php

namespace App\Http\Controllers;

use App\Category;
use App\VideoCategory;

use App\SubCategory;
use App\VideoSubCategory;

use Illuminate\Http\Request;

class VideoCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $archive = Category::all();
        return view('archive_category', compact('archive'));
    }
    /**
     * Display the specified archive.
     *
     * @param  \App\VideoCategory  $videoCategory
     * @return \Illuminate\Http\Response
     */
    public function show($videoCategory)
    {   
        $archive = Category::where('id',Category::categoryID($videoCategory))->first();
        if($archive !== null){
            return view('archive_category', compact('archive'));
        }
        echo "not found";
    }
     /**
     * Display the specified sub archive.
     *
     * @param  \App\VideoCategory $category, $subCategory
     * @return \Illuminate\Http\Response
     */

    public function showSubCategory($category, $subCategory)
    {   
        $archive = SubCategory::where('id', SubCategory::subcategoryID($subCategory))->first();
        if($archive !== null){
            return view('archive_category', compact('archive'));
        }
        echo "not found";
    }

}