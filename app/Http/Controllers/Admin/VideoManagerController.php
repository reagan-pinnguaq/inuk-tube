<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\VideoTag;
use App\VideoCategory;
use Validator;
use App\Http\Rules\ValidKey;
use App\Http\Rules\Compare;

class VideoManagerController extends Controller
{   

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display the outstanding to be validated videos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::where('status', 0)
                  ->orderBy('updated_at', 'ASC')
                  ->paginate(\App\Settings::where('field', 'paginator')->first()->value);
        $status = 0;
        return view('admin.video_manager', compact('videos', 'status'));
    }

    /**
     * Display all videos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $videos = Video::where('status', 1)->orderby('updated_at', 'DESC')->paginate(\App\Settings::where('field', 'paginator')->first()->value);
        $status = 1;
        return view('admin.video_manager', compact('videos', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.video_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = $request->all();
        
        $this->validateKey($data);
        $this->ValidateRelation($data);

        // Create the new video
        $v = new Video;
        $v->type_id = $data['video_type'];
        $v->status = 0;
        $v->key = $v->getVideoKey($data['url'], $v->type_id);
        $v->save();

        if(isset($data['category'])){
        //Create the Video Category
            $categories = array_keys($data['category']);
            foreach($categories as $category){
                $vc = new VideoCategory;
                $vc->video_id = $v->id;
                $vc->category_id = $category;
                $vc->save(); 
            }
        }
        if(isset($data['tag'])){
        //Create the tags
            $tags = array_keys($data['tag']);
            foreach($tags as $tag){
                $vt = new VideoTag;
                $vt->video_id = $v->id;
                $vt->tag_id = $tag;
                $vt->save(); 
            }
        }
        $video = $v->getContents();
        return view('admin.pages.video_confirm',compact('video'));
    }    

    public function confirm(Request $request){
        $data = $request->all();
        
        $video = Video::where('key', $data['key']);
        if(isset($data['confirm']) && $data['confirm'] == true){
            $video->update(['name' =>  $data['title'], 'status' => true], ['aprover_id'] == \Auth::id() );
            $request->session()->flash('message', 'The video has been added and is added to the stie.'); 
            return redirect('/video-manager');
        } else {
            $video->delete();
            $request->session()->flash('message', 'Video was removed. You may try the entry again down below.'); 
            return redirect('/video-manager');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $videoData = Video::find($id);
        return view('admin.pages.video_edit', compact('videoData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        // Doing the delete from update because put push
        // and delete/patch/put methods all throw invalid request method
        // Even using the proper name="_method" & value="DELETE"

        if(isset($data[$data['_token']]) && $data[$data['_token']] == 'DELETE'){
            $this->destroy($id);
            $request->session()->flash('message', 'Video was removed.'); 
            return redirect('/video-manager');
        }

        //Now for update
        $video = Video::find($id);
        $key = $video->getVideoKey($data['url'], $data['video_type']);
        $this->ValidateRelation($data);
        
        if($video->key != $key){
            $this->validateKey($data);
            $video->key = $video->getVideoKey($data['url'], $data['video_type']);
            $video->type_id = $data['video_type'];
        }

        $fields = array_keys($data['stored']);

        foreach($fields as $key){
            switch($key){
                case 'url': case 'video_type': break; //skip these handled above
                case 'tag': case 'category':
                $this->updateRelation($key, $data, $id); 
                break;
                default: $video->$key = $data[$key]; break;
            }
        }
        
        $video->save();
        $request->session()->flash('message', 'Video was updated.');
        return redirect('/video-manager/'. $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function destroy($id)
    {   
        $video = Video::find($id);
        $videoTags = VideoTag::where('video_id', $video->id);
        $videoCategories = VideoCategory::where('video_id', $video->id);

        $videoTags->delete();
        $videoCategories->delete();
        $video->delete();
        
        return redirect('/video-manager');
    }

    private function validateKey($data){
        $v = Validator::make($data, [
            'url' => ['required', 'string', 'max:255', 'url',
            new ValidKey($data['video_type'])
            ]
        ]);
        if($v->fails()){
            //Redirection from a private function
            $this->returnBack($v->errors());
        }
    }

    private function ValidateRelation($data){
        $v = Validator::make($data, [
            'category' => ['required', 'array'],
            'tag' => ['required', 'array']
        ]);
        if($v->fails()){
            //Redirection from a private function
            $this->returnBack($v->errors());
        }
    }

    private function updateRelation($key, $data, $id){
        //insert new entries
        $rel = array_keys($data[$key]);
        foreach($rel as $new){
            if(!isset($data['stored'][$key]) || !in_array($new, $data['stored'][$key])){
                if($key == 'tag'){
                    $vr = new VideoTag;
                    $vr->tag_id = $new;
                } elseif($key == 'category') {
                    $vr = new VideoCategory;
                    $vr->category_id = $new;
                } else {
                    $this->returnBack([0 => 'Error: Could add new '. $key . ' relationships where relationship is :' . $new]);
                }
                $vr->video_id = $id;
                $vr->save();  
            }
        }
        if(isset($data['stored'][$key])){        
        //remove old relation
            foreach($data['stored'][$key] as $old){
                if(!in_array($old, $rel)){
                    if($key == 'tag'){
                        $vr = VideoTag::where('tag_id', $old);
                        $vr->delete();
                    } elseif($key == 'category'){
                        $vr = VideoCategory::where('category_id', $old);
                        $vr->delete();
                    } else {
                        $this->returnBack([0 => 'Error: Could not remove old ' . $key . ' relationships is :' . $new]);
                    }
                }
            }
        }
    }

    private function returnBack($errors = null){
        $resp = redirect()->back()->withErrors($errors);
        \Session::driver()->save();
        $resp->send();
        exit();
    }
}