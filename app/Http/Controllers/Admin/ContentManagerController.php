<?php

namespace App\Http\Controllers\Admin;

use App\LangContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('admin.content_manager');
    }

    /**
     * Show the form for edutubg the headings and persistent
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $items = LangContent::where('page_id', null)->get();
        return view('admin.pages.content_persistent', compact('items'));
    }

    /**
     * Update the persistent and headings
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);

        $keys = array_keys($data);
        
        foreach($keys as $key){
            $entry = LangContent::find($key);
            $item_keys = array_keys($data[$key]);
            foreach($item_keys as $item){
                $entry->$item = $data[$key][$item];
            }
            $entry->save();
        }
        $request->session()->flash('message', 'Entries Updated.');
        return redirect('/content-manager/persistent');
    }
}
