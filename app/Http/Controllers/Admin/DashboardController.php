<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;

class DashboardController extends Controller
{   

    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $count = count(Video::where('status', 0)->get());
        return view('admin.dashboard', compact('count'));
    }

}
