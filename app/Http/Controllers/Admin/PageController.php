<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\LangContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::paginate(\App\Settings::where('field', 'paginator')->first()->value);
        
        return view('admin.page_manager', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.page_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $page = new Page();
        $page->name = \App\Model::slugify($data['title']);
        $page->save();
        
        $this->_initLangContent($page->id, 'page_title' , $data['title']);
        $this->_initLangContent($page->id, 'page_content', $data['content']);  

        return redirect('/content-manager/pages/'. $page->id);
    }

    
    private function _initLangContent($page_id, $field_name, $value)
    {   

        $lang = session('lang');
        $content = new LangContent();
        $content->field = $field_name;
        $content->page_id = $page_id;
        $langs = ['english', 'french', 'inuktitut', $lang ];
        foreach($langs as $lang)
        {
            $content->$lang = $value;
        }
        $content->save();
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id == 1 || $id == 2){
            $page = LangContent::where('page_id', $id)->get();
            ($id == 1 ? $template = 'admin.pages.edit_upload_page' : $template = 'admin.pages.edit_confirm_page'); 
            return view($template, compact('page'));
        } else {
            $page = Page::where('id', $id)->first();
            
            return ($page ? view('admin.pages.edit_normal_page', compact('page')): $this->index());
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $data = $request->all();
        $this->_updatePage($data, $id);
        return redirect('/content-manager/pages/'. $id);
    }

    private function _updatePage($data, $id){
        $lang = session('lang');
        foreach($data as $key => $value)
        {
            if($key != '_token' && $key != 'answer' && $key != 'uAnswer' && $key != 'url' )
            {
                $field = LangContent::find($id)->where('field', $key)->first();
                if($field->$lang != $value)
                {
                    $field->$lang = $value;
                    $field->save();
                }
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {       
        if($id == 1 || $id == 2)
        {
            // DO NOT ALLOW
            $request->session()->flash('message', 'This page is essential and is not allowed to be deleted');
            return redirect('/content-manager/pages');
        }         
        
        $e = \App\Page::where('id', $id)->firstorFail();
        
        if($e && $this->_destoryPageChildren($id))
        {   
            $tmp = new Page();
            ($tmp->destroy($e->id) ? $request->session()->flash('message', $e->name . ' Has been removed as well as all of its translated content') : session()->flash('Error', $e->name . 'Failed to delete'));
        } 
        return redirect('/content-manager/pages/');
    }

    private function _destoryPageChildren($id)
    {
        $d = LangContent::where('page_id', $id)->get();
        
        if($d){
            foreach($d as $content)
            {   
                $tmp = new LangContent();
                if (!$tmp->destroy($content->id))
                {
                    Request::session()->flash('error', ' Failed to delete sub content on ' . $content->id. '.');
                    return false;
                }
            }
        }
        return true;
    }


}
