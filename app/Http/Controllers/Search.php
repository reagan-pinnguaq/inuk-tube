<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Search extends Controller
{
    public function show(Request $request){
        if(!isset($request->all()['query'])){
            return redirect('/');
        }
        $term = $request->all()['query'];
        
        //Video results
        $result = \App\Video::search($term)
        ->paginate(\App\Settings::where('field', 'paginator')
        ->first()->value);

        //Tag results
        $tag = \App\Tag::search($term);
        if($tag !== null){

        }
        //category results
        $categories = \App\Category::search($term)->get();
         
        if($categories->count() == 0){
            $category = null;
        }
       
        return view('page.search', compact('result', 'term', 'categories'));
    }
}