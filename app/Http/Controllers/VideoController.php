<?php

namespace App\Http\Controllers;
use Validator;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Rules\ValidKey;
use App\Http\Rules\Compare;
use App\Page;

class VideoController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($video)
    {   
        $video = Video::where('key', $video)->first();
        if($video->status == true){
            $related = $video->getRelatedVideos();
            $video = $video->getContents();
            return view('page.video', compact('video', 'related'));
        }
        // return 404(); 
    }


    public function showRandom(){
        $video = Video::inRandomOrder()->where('status', true)->first()->getContents();
        return view('page.video', compact('video'));        
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
       $lang = session('lang');
        return view('page.video_create', compact('lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $v = Validator::make($data, [
            'uAnswer' => ['required', 'integer', new Compare($data['answer'])],
            'url' => ['required', 'string', 'max:255', 'url',
            new ValidKey($data['video_type'])
            ],
            'accept' => 'accepted'
        ]);
        if($v->fails()){
            return redirect()->back()->withErrors($v->errors());
        }
        $v = new Video;
        $v->type_id = $data['video_type'];
        $v->status = 0;
        $v->key = $v->getVideoKey($data['url'], $v->type_id);
        $v->save();
        $video = $v->getContents();

        return view('page.video_confirm',compact('video'));
    }

    public function confirm(Request $request){
        $data = $request->all();
        
        $video = Video::where('key', $data['key']);
        if(isset($data['confirm']) && $data['confirm'] == true){
            $video->update(['name' =>  $data['title']]);
            $request->session()->flash('message', 'The video has been added and is awaiting review. You may add another video down below.'); 
            return redirect('/add-video');
        } else {
            $video->delete();
            $request->session()->flash('message', 'Video was removed. You may try the entry again down below.'); 
            return redirect('/add-video');
        }
    }
}
