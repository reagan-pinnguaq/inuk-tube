<?php 

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\Video;
use Alaouy\Youtube\Facades\Youtube;
use Vinkla\Vimeo\Facades\Vimeo;

class ValidKey implements Rule
{
	protected $messages = array(
		"We were unable to validate the video. Please try again later. If this message persists please email developers@pinnguaq.com",
		'Could not parse video key. Please review the video url and hosted site selection submited.',
		'The video you linked to is either already on the site or awaiting aproval.',
		'The url does not seem to be a valid youtube video or is marked private, or is marked to not be shared. Please check the url submited.',
		'The url does not seem to be a valid vimeo video or is marked private, or is marked to not be shared. Please check the url submitted.'
	);
	protected $message;
	protected $video_id;

	function __construct($id)
	{
		$this->message = $this->messages[0];
		$this->video_id = $id;
	}
	
	public function passes($attribute, $value)
	{
		$key = new Video;
		$key = $key->getVideoKey($value, $this->video_id);
		
		if($this->notFalse($key) && $this->validateKey($key) && $this->notStoragedKey($key)){
				return true;
			}
		return false;
	}

	public function message()
	{
		return $this->message;
	}


	protected function notFalse($key){
		if($key == false){
			$this->message = $this->messages[1];
			return false;
		}
		return true;
	}

	protected function notStoragedKey($key)
	{
		$query = Video::where('key', $key)->first();
		if($query == null){
			return true;
		}
		$this->message = $this->messages[2];
		return false;
	}

	protected function validateKey($key)
	{
		switch($this->video_id){
			case 1: return $this->validateYoutube($key); break;
			case 2: return $this->validateVimeo($key); break;
			break;
			default: return false; break;
		}
	}

	protected function validateYoutube($key){
		$result = Youtube::getVideoInfo($key);
		if($result == false): 
			$this->message = $this->messages[3];
			return false; 
		endif;
		return true;
	}

	protected function validateVimeo($key){
		$result = Vimeo::request('/videos/' . $key);
		if(isset($result['body']['name']) && $result['body']['name'] != ''){
			return true;
		}
		$this->message = $this->messages[4];
		return false;
	}
}