<?php 

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;;

class Compare implements Rule
{
	public $e;
	protected $message = "Please try again. The answer you provided to the math question does not validate.";
	function __construct($compare)
	{
		$this->e = $compare;
	}
	
	public function passes($attribute, $value)
	{
		if($value == $this->e){
			return true;
		}
		return false;
	}

	public function message()
	{
		return $this->message;
	}

}
