<?php

namespace App;
use App\Model;
// use 
class LangContent extends Model
{
    //
    function page(){
        return $this->belongsTo(Page::class);
    }

    function field($field){
        $lang = session('lang');
        return $this->where('field', $field)->first()->$lang;
    }
    
}
