<?php

namespace App;
use App\Model;

class VideoSubCategory extends Model
{
    function parent(){
        return $this->belongsTo(Category::class);
    }

    function video(){
        return $this->belongsTo(Video::class)->first();
    }
}
