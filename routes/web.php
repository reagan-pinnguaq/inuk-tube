<?php

use Illuminate\Routing\Redirector;

Auth::routes();
Route::get('/', function () {
    return view('page.home');
});

/////////////////////////////////////////////
//                 Front End               //
/////////////////////////////////////////////

// Route::get('/language/{lang}', 'LanguageController@set');

// Route::get('/videos/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavRandomVideo')->first()->english), 'VideoController@showRandom');
// // Route::get('/videos/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavRandomVideo')->first()->french), 'VideoController@showRandom');
// Route::get('/videos/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavRandomVideo')->first()->inuktitut), 'VideoController@showRandom');

// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavCategories')->first()->english), 'VideoCategoryController@index');
// // Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavCategories')->first()->french), 'VideoCategoryController@index');
// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavCategories')->first()->inuktitut), 'VideoCategoryController@index');

// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavCategories')->first()->english) . '/{wildcard}', 'VideoCategoryController@show'); #archiv
// // Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavCategories')->first()->french) . '/{wildcard}', 'VideoCategoryController@show'); #archiv
// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavCategories')->first()->inuktitut) . '/{wildcard}', 'VideoCategoryController@show'); #archiv

// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavAddVideo')->first()->english), 'VideoController@create');
// // Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavAddVideo')->first()->french), 'VideoController@create');
// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavAddVideo')->first()->inuktitut), 'VideoController@create');

// Route::post('/upload', 'VideoController@store');
// Route::post('/confirm', 'VideoController@confirm');

// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavSearch')->first()->english), 'Search@show');
// // Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavSearch')->first()->french), 'Search@show');
// Route::get('/' . \App\Model::slugify(\App\LangContent::where('field', 'LangNavSearch')->first()->inuktitut), 'Search@show');


/////////////////////////////////////////////
//                  Back End               //
/////////////////////////////////////////////

Route::get('/dashboard', 'Admin\DashboardController@index');
// Route::post('/dashboard', 'Auth\LoginController');

//  Video Manager
Route::get('/video-manager', 'Admin\VideoManagerController@index')->name('video-manager');
Route::post('/video-manager', 'Admin\VideoManagerController@store');
Route::get('/video-manager/all', 'Admin\VideoManagerController@show');
Route::get('/video-manager/create', 'Admin\VideoManagerController@create');
Route::post('/video-manager/confirm', 'Admin\VideoManagerController@confirm');
Route::post('video-manager/{id}', 'Admin\VideoManagerController@update');
//Route::delete('video-manager/{id}', 'Admin\VideoManagerController@delete');
Route::get('/video-manager/{id}', 'Admin\VideoManagerController@edit');

//  Content Manager
Route::get('/content-manager', 'Admin\ContentManagerController@index');
Route::get('/content-manager/persistent', 'Admin\ContentManagerController@edit');
Route::post('/content-manager/persistent', 'Admin\ContentManagerController@update');

Route::get('/content-manager/pages', 'Admin\PageController@index');
Route::get('/content-manager/pages/create', 'Admin\PageController@create');
Route::post('/content-manager/pages', 'Admin\PageController@store');
Route::get('/content-manager/pages/{$id}', 'Admin\PageController@edit');
Route::get('/content-manager/pages/{id}', 'Admin\PageController@edit');
Route::post('/content-manager/pages/{id}', 'Admin\PageController@update');
Route::post('/content-manager/pages/{id}/delete', 'Admin\PageController@destroy');

// Setting Manager
Route::get('/setting-manager', 'Admin\SettingManagerController@edit');
Route::post('/setting-manager', 'Admin\SettingManagerController@update');

/////////////////////////////////////////////
//                 Last Chance             //
/////////////////////////////////////////////

// Route::get('/{wildcard}', function ($wildcard) { 
//     if(null !== App\Video::where('key', '=', $wildcard)->first()){
//         $controller = app()->make('\App\Http\Controllers\VideoController');
//         return $controller->callAction('show', compact('wildcard')) ;
//     } elseif (null !== App\Page::where('name', '=', $wildcard)->first()){
//         $controller = app()->make('\App\Http\Controllers\PageController');
//         return $controller->callAction('show', compact('wildcard')) ;
//     } else {
//         return App::abort(404);
//     }
// });