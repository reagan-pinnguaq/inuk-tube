# Setup on config for Inuktitube #

Create you .env file from .env.example. This project assumes mysql for other database needs. Make sure to set `SCOUT_DRIVER=mysql` in here as well.

### Composer ### 

run `composer install` to install all of laravel's dependencies as well as our own dependincies. As of this version of laravel auto generate should take care of all the config and aliases.

## Database & Seaching ##

To start we need to populate the database with seeds. This is presampled content that Talia Metuq supplied. However before that... we must disable the routes because they are dynamic routes.

open up routes/web.php

comment out all the front end routes.

if you get an error about package discovery after installing all of the dependncies via composer please run

`php artisan package:discover`

Now run:

`php artisan migrate:fresh --seed`

Then from there we need to populate the data for scout. 

`php artisan scout:mysql-index App\\Video`

`php artisan scout:mysql-index App\\Category`

Then uncomment the front end routes.

Next you will get an error about no encryption key. To fix that run.

'php aritsan key:generate`

`php artisan config:cache`

And fin.